/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ak.finance.web.rest.vm;
